# Using WSL (Windows Subsystem for Linux)

## From wondows console

### How to change the root password in WSL

1. Open console
2. Swith to root user '''wsl -u root'''
3. change the root password '''passwd'''
4. verify the new password '''su -'''

*alternative method: '''swl -u root passwd [username]'''

### Update WSL dependencies

1. open wsl terminal
2. sudo apt update
3. sudo apt upgrade -y

### enable Copy/Paste

1. Right click the terminal head bar
2. Cick properties
3. Check 'Use CTRL + SHIFT + C/V as Copy/Paste
